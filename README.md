# Software Studio 2021 Spring
## Assignment 01 Web Canvas

### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---


### How to use 
    首先點開網頁，你會看到左邊是功能區，右邊則是畫布主體
    左上角的font跟size是給打字用的
    接著接著往右看到的黑色框框是選色器，他的右邊則是筆刷粗細調整
    接著左邊區看到八個圓形按鈕，由上而下分別是：「畫筆、擦布、重置、text、圓
    、矩形、三角形、下載、上傳、、直線、、上一步、下一步」

### Function description

    首先,按鍵被點擊時，都會觸發buttonset(str)，在buttonset中透過str來確定按了什麼按鈕，在個別呼叫對應函示去做事。
    我設定在點擊btn後就會去改變游標圖示，所以在buttonset裡面就加入 canvas.cursor = (XXXX)的語法。

    畫筆的部分，我用到mousedown/mousemove/mouseup的eventlistener，去對游標經過的點畫畫，mouseup時去removerlistener。

    擦布的部分，也蠻類似的，但是是在游標經過的地方做clearRect();

    text的部分，我一開始在html裡面就有放一個input 框框但是我把它藏在canvas下面，只要text點擊到canvas上面我就把他的zindex拉上來left top也改成游標位置這樣就能打字了，打字完成的字串再給filltext寫入canvas    。

    畫形狀的部分，為了能看到預覽圖，當我還沒mousup之前我會把他畫在原先的canvas上的preview_canvas也就是canvas疊兩層，當mouseup觸發，就把preview_canvas remove，把最終版畫入canvas。

    上一步、下一步的部分，只要在每次畫完之後，就紀錄一次state並push到history裡面，當上一步、下一步被clicked我就呼叫history.back();history.forward();這樣就實現了。

    下載的部分，直接create一個<a>，link.href = url設定url，最後把我的按鍵的id跟<a>的綁定就完成了。

    


### Gitlab page link

   https://108062134.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    助教手下留情

<style>
table th{
    width: 100%;
}
</style>
