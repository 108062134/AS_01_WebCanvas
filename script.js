try{
    var canvas = document.getElementById("mycanvas");
    var canvas_context = canvas.getContext('2d');
    var color_change = document.getElementById("controlpart");
    color_change.addEventListener('input',setcolor);
    color_change.addEventListener('change',setcolor);
    var pen = document.getElementById('penwidth');
    var size = document.getElementById('size');
    size.addEventListener('change',setsize);
    pen.addEventListener('change',penwidthcontrol);
    var neW = document.getElementById('new');
    neW.addEventListener('click',setfont);
    var impact = document.getElementById('impact');
    impact.addEventListener('click',setfont);
    var arial = document.getElementById('arial');
    arial.addEventListener('click',setfont);
    let state = canvas_context.getImageData(0,0,canvas.width,canvas.height);
    window.history.pushState(state,null);
    window.addEventListener('popstate',state_handler,false);
    canvas_context.lineWidth = 8;
    var img;
    function state_handler(event){
      canvas_context.clearRect(0,0,canvas.width,canvas.height);
  
      if(event.state){
        canvas_context.putImageData(event.state,0,0);
      }
    }
  
    function penwidthcontrol(event){
      canvas_context.lineWidth = this.value;
    }
    function getcanvaspos(c,e){
        var rec = c.getBoundingClientRect();
        var x,y;
        return{
          x:e.clientX - rec.left,
          y:e.clientY - rec.top
        };
    }
    function Mouseup(event){
      canvas.removeEventListener('mousemove',getmouse);
      console.log("up");
      let state = canvas_context.getImageData(0, 0, canvas.width, canvas.height);
      window.history.pushState(state, null);
    }
    function getmouse(event){
      var pos = getcanvaspos(canvas,event);
      //img.style.left = event.clientX +'px';
      //img.style.top = event.clientY +'px';     // 如果加入這兩行 畫筆會失控 (抓不到mouseup)
      canvas_context.lineTo(pos.x,pos.y);
      canvas_context.stroke();
      canvas.addEventListener('mouseup',Mouseup);
    }
    function Mousedown(event){ 
      var pos = getcanvaspos(canvas,event);
      event.preventDefault();
      canvas_context.beginPath();
      canvas_context.moveTo(pos.x,pos.y);
      canvas.addEventListener('mousemove',getmouse); 
      
    }
  /*     pen       */
    function pencontrol(){
      canvas_context.lineWidth = 8;
      canvas.addEventListener('mousedown',Mousedown);
    }
  /*     reset     */
    function reset(){
      canvas_context.clearRect(0,0,canvas.width,canvas.height);
    }
  /*     eraser    */
    function erasing(event){
      var cur = getcanvaspos(canvas,event);
      canvas_context.clearRect(cur.x-5,cur.y-5,canvas_context.lineWidth,canvas_context.lineWidth);
      canvas.addEventListener('mouseup',erasing_terminate);
    }
    function erasing_terminate(event){
      let state = canvas_context.getImageData(0, 0, canvas.width, canvas.height);
      window.history.pushState(state, null);
      canvas.removeEventListener('mousemove',erasing);
      canvas.removeEventListener('mouseup',erasing_terminate);
    }
    function start_erase(event){
      var cur = getcanvaspos(canvas,event);
      canvas_context.clearRect(cur.x-5,cur.y-5,canvas_context.lineWidth,canvas_context.lineWidth);
      canvas.addEventListener('mousemove',erasing);
    }
    function erase(){
      canvas.addEventListener('mousedown',start_erase);
    }
    var tmp = '10px' ,font = 'PMingLiU';
    function setsize(event){
      tmp = "";
      tmp = this.value;
      tmp += 'px';
      tmp += ' ';
      tmp += font;
      console.log(tmp);
      canvas_context.font = tmp;
    }
    function setfont(event){
    //  console.log(this.id);
      if(this.id == 'new') font = 'PMingLiU';
      else if(this.id == 'impact') font = 'impact'
      else if(this.id == 'arial') font = 'arial';
      //console.log(font);
      var i =0;
      var temp="";
      while(tmp[i] != 'p'){
        temp += tmp[i];
        i++;
      }
      tmp = temp;
      tmp += 'px';
      tmp += ' ';
      tmp += font;
      canvas_context.font = tmp;
      
    }
    function setcolor(event){
      canvas_context.strokeStyle = color_change.value;
    }
    function upload(){
        var id = document.getElementById('upload');
        var inputObj = document.createElement('input');
        inputObj.addEventListener('change',readFile,false);
        inputObj.type = 'file';
        inputObj.accept = 'image/*';
        inputObj.id = id;
        inputObj.click();
    }
    function readFile(){
      var file = this.files[0];                //獲得input輸入的圖片
      var reader = new FileReader();
      reader.readAsDataURL(file);              //轉化成base64資料型別
      reader.onload = function(e){
        drawToCanvas(this.result);
      }
    }
    function drawToCanvas(imgData){
      var img = new Image;
      img.src = imgData;
      img.onload = function(){            //必須onload之後再畫
        canvas_context.drawImage(img,0,0,150,200);
        strDataURI = canvas.toDataURL();  //獲取canvas base64資料
      }
      let state = canvas_context.getImageData(0, 0, canvas.width, canvas.height);
      window.history.pushState(state, null);
    }
    
    function key(event){
      txt = event.key;
    }
    var inputstr ="";
    var pos;
    function terminate(event){
      inputstr = "";
      canvas.removeEventListener('mousedown',type);
      document.body.removeEventListener('keypress',readtext);
      document.body.removeEventListener('mousedown',terminate);
    }
    function readtext(event){
      console.log(box.value);
      inputstr = box.value;
      document.body.addEventListener('mousedown',terminate);
      if(event.keyCode == 13){
         box.style.zIndex = -3;
         document.body.removeEventListener('keypress',readtext);
         canvas.removeEventListener('mousedown',type);
         canvas_context.fillText(inputstr,pos.x,pos.y);
         box.value ="";
      }
    }
    var box
    function type(event){ 
      pos = getcanvaspos(canvas,event);
      box = document.getElementById('box');
      box.style.zIndex = 3;
      box.style.left = event.clientX +'px';
      box.style.top = event.clientY +'px';
      console.log("ww");
      document.body.addEventListener('keypress',readtext);
    }
    function end_point_collect(event){
      var px,py;
      var pos = getcanvaspos(canvas,event);
      canvas.removeEventListener('mousemove',draw_circle);
    }
    var startpx;
    var startpy;
    function draw_circle(event){
      var cur = getcanvaspos(canvas,event);
      var x = (startpx+cur.x)/2;
      var y = (startpy+cur.y)/2;
      var rad = Math.pow((cur.x-x)*(cur.x-x)+(cur.y-y)*(cur.y-y),1/2);
      canvas_context.beginPath();
      canvas_context.arc(x,y,rad,0,360,false);
      canvas_context.stroke();
      canvas_context.closePath();
      preview_canvas.removeEventListener('mousemove',drawing_preview);
      document.body.removeChild(preview_canvas);
      let state = canvas_context.getImageData(0, 0, canvas.width, canvas.height);
      window.history.pushState(state, null);
      console.log("draw_circle");
    }
    function draw_rec(event){
      var cur = getcanvaspos(canvas,event);
      var x = (startpx+cur.x)/2;
      var y = (startpy+cur.y)/2;
      var width = Math.abs(cur.x - startpx);
      var height = Math.abs(cur.y - startpy);
      canvas_context.strokeRect(startpx,startpy,width,height);
      canvas_context.closePath();
      preview_canvas.removeEventListener('mousemove',drawing_preview);
      document.body.removeChild(preview_canvas);
      let state = canvas_context.getImageData(0, 0, canvas.width, canvas.height);
      window.history.pushState(state, null);
    }
    function draw_line(event){
      var cur = getcanvaspos(canvas,event);
      canvas_context.moveTo(startpx,startpy);
      canvas_context.lineTo(cur.x,cur.y);
      canvas_context.stroke();
      canvas_context.closePath();
      preview_canvas.removeEventListener('mousemove',drawing_preview);
      document.body.removeChild(preview_canvas);
      let state = canvas_context.getImageData(0, 0, canvas.width, canvas.height);
      window.history.pushState(state, null);
    }
    function draw_tri(event){
      var cur = getcanvaspos(canvas,event);
      canvas_context.moveTo(startpx,startpy);
      canvas_context.lineTo(cur.x,cur.y);
      canvas_context.lineTo((cur.x+startpy)/2,(cur.y+startpx)/2);
      canvas_context.lineTo(startpx,startpy);
      canvas_context.stroke();
      canvas_context.closePath();
      preview_canvas.removeEventListener('mousemove',drawing_preview);
      document.body.removeChild(preview_canvas);
      let state = canvas_context.getImageData(0, 0, canvas.width, canvas.height);
      window.history.pushState(state, null);
      console.log("draw_tri");
    }
    var shape = 0;
    var preview_canvas,pctx;
    function data_collect(event){
      /*     找到起始點        */
      var cur = getcanvaspos(canvas,event);
      startpx = cur.x;
      startpy = cur.y;
      console.log("data_colect");
      /*     建立一層預覽層     */
      preview_canvas = document.createElement('canvas');
      preview_canvas.setAttribute('width','800px');
      preview_canvas.setAttribute('height','600px');
      preview_canvas.style.position = 'absolute';
      preview_canvas.style.zIndex = 1;
      preview_canvas.style.left = '506.5px';
      preview_canvas.style.top = '108px';
      document.body.appendChild(preview_canvas);
      pctx = preview_canvas.getContext('2d');
      pctx.lineWidth = canvas_context.lineWidth;
      preview_canvas.addEventListener('mousemove',drawing_preview);

      if(shape == 0)
        preview_canvas.addEventListener('mouseup',draw_circle);
      else if(shape == 1)
        preview_canvas.addEventListener('mouseup',draw_rec);
      else if(shape == 2)
        preview_canvas.addEventListener('mouseup',draw_tri);
      else if(shape == 3)
        preview_canvas.addEventListener('mouseup',draw_line);
    }
    function drawshape(){
      canvas.addEventListener('mousedown',data_collect);
    }
    
    function drawing_preview(event){
      if(shape == 0){                        //        畫圓形
        pctx.clearRect(0,0,canvas.width,canvas.height);
        var cur = getcanvaspos(canvas,event);
        var x = (startpx+cur.x)/2;
        var y = (startpy+cur.y)/2;
        var rad = Math.pow((cur.x-x)*(cur.x-x)+(cur.y-y)*(cur.y-y),1/2);
        pctx.beginPath();
        pctx.arc(x,y,rad,0,360,false);
        pctx.stroke();
        pctx.closePath();
        console.log("draw_preview_circle");
      }else if(shape == 1){                   //        畫矩形
        pctx.clearRect(0,0,canvas.width,canvas.height);
        var cur = getcanvaspos(canvas,event);
        var x = (startpx+cur.x)/2;
        var y = (startpy+cur.y)/2;
        var width = Math.abs(cur.x - startpx);
        var height = Math.abs(cur.y - startpy);
        pctx.strokeRect(startpx,startpy,width,height);
        pctx.closePath();
      }else if(shape == 2){                    //        畫三角形
        pctx.clearRect(0,0,canvas.width,canvas.height);
        var cur = getcanvaspos(canvas,event);
        pctx.beginPath();
        pctx.moveTo(startpx,startpy);
        pctx.lineTo(cur.x,cur.y);
        pctx.lineTo( (cur.x+startpy)/2,(cur.y+startpx)/2);
        pctx.lineTo(startpx,startpy);
        pctx.stroke();
        pctx.closePath();
        console.log("draw_preview_tri");
      }else if(shape == 3){                    //        畫直線
        pctx.clearRect(0,0,canvas.width,canvas.height);
        var cur = getcanvaspos(canvas,event);
        pctx.beginPath();
        pctx.moveTo(startpx,startpy);
        pctx.lineTo(cur.x,cur.y);
        pctx.stroke();
        pctx.closePath();
      }
    }
    
    
    
  /*     button_control    */
    function buttonset(str){
      
      if(str == "eraser"){
          canvas.removeEventListener('mousedown',Mousedown,false);
          canvas.removeEventListener('mouseup',draw_rec);
          canvas.removeEventListener('mousedown',drawshape);
          canvas.removeEventListener('mouseup',draw_line,false);
          canvas.removeEventListener('mousedown',data_collect,false);
          canvas.removeEventListener('mouseup',draw_circle);
          canvas.style.cursor = 'url("eraser.png"), auto';
          erase();
      }else if(str == "pen"){
          canvas.removeEventListener('mousedown',data_collect,false);
          canvas.removeEventListener('mouseup',draw_rec);
          canvas.removeEventListener('mousedown',drawshape);
          canvas.removeEventListener('mouseup',draw_line,false);
          canvas.removeEventListener('mouseup',draw_circle);
          canvas.removeEventListener('mousedown',start_erase);
          canvas.style.cursor = 'url("pen.png"), auto';
          pencontrol();
          setcolor('black');
      }else if(str == "reset"){
          canvas.removeEventListener('mousedown',Mousedown,false);
          canvas.removeEventListener('mouseup',draw_rec);
          canvas.removeEventListener('mouseup',draw_line,false);
          canvas.removeEventListener('mousedown',data_collect,false);
          canvas.removeEventListener('mouseup',draw_circle);
          canvas.removeEventListener('mousedown',start_erase);
          reset();
      }else if(str == 'upload'){
          canvas.removeEventListener('mousedown',Mousedown,false);
          canvas.removeEventListener('mouseup',draw_rec);
          canvas.removeEventListener('mouseup',draw_line,false);
          canvas.removeEventListener('mousedown',data_collect,false);
          canvas.removeEventListener('mouseup',draw_circle);
          canvas.removeEventListener('mousedown',start_erase);
          upload();
      }else if(str == 'text'){
         canvas.removeEventListener('mousedown',Mousedown,false);
         canvas.removeEventListener('mouseup',draw_rec);
         canvas.removeEventListener('mouseup',draw_line,false);
         canvas.removeEventListener('mousedown',data_collect,false);
         canvas.removeEventListener('mouseup',draw_circle);
         canvas.addEventListener('mousedown',type);
         canvas.removeEventListener('mousedown',start_erase);
         canvas.style.cursor = 'url("text.png"), auto';
      }else if(str == 'save'){
        canvas.removeEventListener('mousedown',data_collect,false);
        canvas.removeEventListener('mouseup',draw_rec);
        canvas.removeEventListener('mouseup',draw_line,false);
        canvas.removeEventListener('mouseup',draw_circle);
        canvas.removeEventListener('mousedown',start_erase);
        const blob = canvas.toBlob(blob =>{
          const url = URL.createObjectURL(blob);
          var id = document.getElementById('download');
          const link = document.createElement('a');
          link.href = url;
          link.id = id;
          link.download = 'my_painting.png';
          link.click();
        })
      }else if(str == 'circle'){
        shape = 0;
        canvas.removeEventListener('mouseup',draw_line,false);
        canvas.removeEventListener('mousedown',Mousedown,false);
        canvas.removeEventListener('mouseup',draw_rec);
        canvas.removeEventListener('mousedown',drawshape);
        canvas.removeEventListener('mousedown',start_erase);
        canvas.style.cursor = 'url("circle.png"), auto';
        drawshape();
      }else if(str == 'rectangle'){
        shape = 1;
        canvas.removeEventListener('mouseup',draw_line,false);
        canvas.removeEventListener('mousedown',Mousedown,false);
        canvas.removeEventListener('mouseup',draw_circle);
        canvas.removeEventListener('mousedown',drawshape);
        canvas.removeEventListener('mousedown',data_collect,false);
        canvas.removeEventListener('mousedown',start_erase);
        canvas.style.cursor = 'url("rectangle.png"), auto';
        drawshape();
      }else if(str == 'next'){
        history.forward();
      }else if(str == 'back'){
        history.back();
        console.log("back");
      }else if(str == 'line'){
        canvas.removeEventListener('mousedown',Mousedown,false);
        canvas.removeEventListener('mouseup',draw_circle);
        canvas.removeEventListener('mousedown',drawshape);
        canvas.removeEventListener('mousedown',data_collect,false);
        canvas.removeEventListener('mousedown',start_erase);
        canvas.style.cursor = 'url("line.png"), auto';
        shape = 3;
        drawshape();
      }else if(str == 'triangle'){
        canvas.removeEventListener('mousedown',Mousedown,false);
        canvas.removeEventListener('mouseup',draw_circle);
        canvas.removeEventListener('mousedown',drawshape);
        canvas.removeEventListener('mousedown',start_erase);
        canvas.removeEventListener('mousedown',data_collect,false);
        canvas.style.cursor = 'url("triangle.png"), auto';
        shape = 2;
        drawshape();
      }
    }
  
    
     
     
  
  }catch(err){
      console.log(err);
  }
  
